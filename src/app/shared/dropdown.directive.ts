import {
  Directive,
  ElementRef,
  Renderer2,
  ViewContainerRef,
  Input,
  HostListener,
  HostBinding,
  Output
   } from '@angular/core';

@Directive({
  selector: '[appDropdown]'
})
export class DropdownDirective {

  @HostBinding('class.open') trigger: boolean = false;

  // @HostListener('click') onClick() {
  //     // this.renderer.addClass(this.vcRef.element.nativeElement, 'open');
  //     this.trigger = !this.trigger;
  // }

  @HostListener('document:click', ['$event.target'])
  public onClick(targetElement) {
      const clickedInside = this.elRef.nativeElement.contains(targetElement);

      if (!clickedInside) {
        this.trigger = false;
      } else {
        this.trigger = !this.trigger;
      }
  }

  // @HostListener('mouseleave') mouseLeave() {
  //     // this.renderer.removeClass(this.vcRef.element.nativeElement, 'open');
  //     this.trigger = !this.trigger;
  // }

  constructor(
    private elRef: ElementRef,
    private vcRef: ViewContainerRef,
    private renderer: Renderer2) { }

}
