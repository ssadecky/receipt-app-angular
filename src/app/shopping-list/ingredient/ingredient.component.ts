import { Component, OnInit, Input } from '@angular/core';
import { Ingredient } from '../../shared/ingredient.model';
import { ShoppingListService } from '../../core/shoppinglist.service';

@Component({
  selector: 'app-ingredient',
  templateUrl: './ingredient.component.html',
  styleUrls: ['./ingredient.component.scss']
})
export class IngredientComponent implements OnInit {

  @Input() ingredient: Ingredient;

  constructor( private shoppingListService: ShoppingListService) { }

  ngOnInit() { }

  removeIngredient() {
    this.shoppingListService.removeIngredient( this.ingredient );
    console.log('got to removeIngredient()');
  }

  editIngredient(data: any) {
    console.log(data);
    this.shoppingListService.editIngredient( this.ingredient, data );
  }



}
