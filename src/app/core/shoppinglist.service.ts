import { EventEmitter } from '@angular/core';
import { Ingredient } from '../shared/ingredient.model';

export class ShoppingListService {
  ingredientsChanged = new EventEmitter<Ingredient[]>();

  ingredients: Ingredient[] = [
    new Ingredient('Peaches', 5),
    new Ingredient('Milk', 1)
  ];

  getIngredients() {
    return this.ingredients.slice();
  }

  addIngredient( ingredient: Ingredient ) {
    this.ingredients.push( ingredient );
    this.ingredientsChanged.emit(this.ingredients.slice());
  }

  addIngredients(ingredients: Ingredient[]) {
    this.ingredients.push(...ingredients);
    this.ingredientsChanged.emit(
      this.ingredients.slice()
    );
  }

  removeIngredient( ingredient: Ingredient ) {
    this.ingredients.splice( this.ingredientPosition(ingredient) , 1);
    this.ingredientsChanged.emit(this.ingredients);
  }

  editIngredient( ingredient: Ingredient, newVelues: any ) {
    Object.keys(newVelues)
    .filter(key => !!newVelues[key])
    .forEach(key => {
      ingredient[key] = newVelues[key];
    });
  }

  ingredientPosition( ingredient: Ingredient ) {
    return this.ingredients.indexOf( ingredient );
  }

}
