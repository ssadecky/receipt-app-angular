import { Component, OnInit, Input } from '@angular/core';

import { Recipe } from '../recipe.model';
import { RecipeService } from '../../core/recipe.service';
import { ToggleService } from '../../shared/toggle.service';


@Component({
  selector: 'app-recipe-detail',
  templateUrl: './recipe-detail.component.html',
  styleUrls: ['./recipe-detail.component.scss']
})
export class RecipeDetailComponent implements OnInit {

  @Input() recipe: Recipe;

  constructor( private recipeService: RecipeService,
      private toggleService: ToggleService  ) { }

  ngOnInit() {
  }

  onAddToShopList(destination: string) {
    this.recipeService.addToShoppingList(this.recipe.ingredients);
    // Here be modal service(toggle-added) if be possible or time.
  }

}
