import { Component, OnInit, ViewChild, EventEmitter, ElementRef, Input, Output} from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { Recipe } from '../recipe.model';
import { Ingredient } from '../../shared/ingredient.model';
import { ToggleService } from '../../shared/toggle.service';
import { RecipeService } from '../../core/recipe.service';

@Component({
  selector: 'app-recipe-list',
  templateUrl: './recipe-list.component.html',
  styleUrls: ['./recipe-list.component.scss'],
  providers: [ToggleService]
})
export class RecipeListComponent implements OnInit {

  @ViewChild('recipeInputs') inputs: ElementRef;
  @ViewChild('recipeName') recipeName: ElementRef;
  @ViewChild('recipeDesc') recipeDesc: ElementRef;
  @ViewChild('imageUrl') imageUrl: ElementRef;
  @ViewChild('ingredientName') ingredientName: ElementRef;
  @ViewChild('ingredientAmount') ingredientAmount: ElementRef;

  recipesChanged = new EventEmitter<Recipe[]>();

  recipes: Recipe[] = [];

  ingredient: Ingredient = { name: '', amount: null };
  @Input() ingredients: Ingredient[] = [this.ingredient];
  

  clearInputs():void {
    this.recipeName.nativeElement.value =
    this.recipeDesc.nativeElement.value =
    this.imageUrl.nativeElement.value = '';
  }

  constructor(
    private togglingService: ToggleService ,
    private recipeService: RecipeService ) { }

  ngOnInit() {
    this.recipes = this.recipeService.getRecipes();
    this.recipeService.recipesChanged
      .subscribe(
          (recipes: Recipe[]) => {
            this.recipes = recipes;
          }
        );
  }

  addToIngredientsArray(index) {
    let i = index;
    if( i === undefined ) i = 0;

    this.ingredient.name = '';
    this.ingredient.amount = null;
    this.ingredients.push(this.ingredient);

    console.log(this.ingredients);
  }

  processIngredients() {
    console.log(this.ingredientName);
  }

  // addRecipe(name: string, desc: string, url: string, ingredient: Ingredient[]) {
  //   console.log(Object.values(this.ingredients));
  //   this.recipeService.addRecipe( new Recipe(name, desc, url, ingredient) );
  // }

}
