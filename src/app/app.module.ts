import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { ShoppingListComponent } from './shopping-list/shopping-list.component';
import { ListEditComponent } from './shopping-list/list-edit/list-edit.component';
import { IngredientComponent } from './shopping-list/ingredient/ingredient.component';
import { RecipeBookComponent } from './recipe-book/recipe-book.component';
import { RecipeDetailComponent } from './recipe-book/recipe-detail/recipe-detail.component';
import { RecipeComponent } from './recipe-book/recipe/recipe.component';
import { RecipeListComponent } from './recipe-book/recipe-list/recipe-list.component';
import { RecipeItemComponent } from './recipe-book/recipe-list/recipe-item/recipe-item.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { ControlsComponent } from './shopping-list/ingredient/controls/controls.component';
import { Routes, RouterModule } from '@angular/router';

// Directives
import { DropdownDirective } from './shared/dropdown.directive';
import { ToggleDirective } from './shared/toggle.directive';

// Material theme components and assets
import {NoopAnimationsModule} from '@angular/platform-browser/animations';

import {MatListModule} from '@angular/material/list';
import {MatButtonModule} from '@angular/material/button';
import {MatInputModule} from '@angular/material/input';

// Service
import { DataService } from './shared/data.service';
import { RecipeService } from './core/recipe.service';
import { ShoppingListService } from './core/shoppinglist.service';

const appRoutes: Routes = [
  { path: 'recipes', component: RecipeBookComponent },
  { path: 'shopping-list', component: ShoppingListComponent },
  { path: '', component: RecipeBookComponent }
];



@NgModule({
  declarations: [
    AppComponent,
    ShoppingListComponent,
    ListEditComponent,
    IngredientComponent,
    RecipeBookComponent,
    RecipeDetailComponent,
    RecipeItemComponent,
    RecipeListComponent,
    RecipeComponent,
    HeaderComponent,
    FooterComponent,
    DropdownDirective,
    ToggleDirective,
    ControlsComponent
  ],
  imports: [
    BrowserModule,
    NoopAnimationsModule,
    MatListModule,
    MatButtonModule,
    MatInputModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [RecipeService, ShoppingListService],
  bootstrap: [AppComponent]
})
export class AppModule { }

export class PizzaPartyAppModule { }
