import { Component, OnInit } from '@angular/core';

import { Ingredient } from '../shared/ingredient.model';
import { ShoppingListService } from '../core/shoppinglist.service';
import { ToggleService } from '../shared/toggle.service';

@Component({
  selector: 'app-shopping-list',
  templateUrl: './shopping-list.component.html',
  styleUrls: ['./shopping-list.component.scss']
})
export class ShoppingListComponent implements OnInit {

  ingredients: Ingredient[] = [];
  showControlsToggler: boolean = true;

  constructor(
    private shoppingListService: ShoppingListService,
    private toggleService: ToggleService
    ) { }

  ngOnInit() {
    this.ingredients = this.shoppingListService.ingredients;
    this.shoppingListService.ingredientsChanged
      .subscribe((ingredients: Ingredient[]) => {
        this.ingredients = ingredients;
      }
    )
  }

}
