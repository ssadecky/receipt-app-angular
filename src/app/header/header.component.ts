import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  @Output() getMenuItem = new EventEmitter<string>();
  menuItem: string = '';

  show(name) {
    this.menuItem = name;
    this.getMenuItem.emit(this.menuItem);
  }

  constructor() { }

  ngOnInit() {
  }

}
