export class ToggleService {

  public menuName: string = 'recipe';
  public isRecipeMenuShown: boolean = false;
  public toggle: boolean = false;

  toggleMenuItems( menuName: string ) {
    this.menuName = menuName;
  }

  toggleRecipeMenu() {
    this.isRecipeMenuShown = !this.isRecipeMenuShown;
  }

  defaultToggle():void {
    this.toggle = !this.toggle;
  }

}
