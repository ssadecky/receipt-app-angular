import {
  Component,
  OnInit,
  Input,
  EventEmitter,
  Output,
  ViewChild
} from '@angular/core';

import { Ingredient } from '../../../shared/ingredient.model';

@Component({
  selector: 'app-controls',
  templateUrl: './controls.component.html',
  styleUrls: ['./controls.component.scss'],
})
export class ControlsComponent implements OnInit {

  @Output() removeTrigger = new EventEmitter<any>();
  @Output() confirmTrigger = new EventEmitter<any>();
  @Input() ingredientSelected;
  @ViewChild('toggleControls') toggleControls;
  selectedData: Ingredient = {name: '', amount: 0};

  constructor() { }

  ngOnInit() {
  }

  onRemove() {
    this.removeTrigger.emit(null);
  }
  onEditSubmit( name: string, amount: number ) {
    this.confirmTrigger.emit({name, amount});
    this.toggleControls.trigger = false;
  }
  onEdit() {
    this.selectedData.name = name;
    this.selectedData.amount = amount;
    console.log(this.selectedData);
  }

}
