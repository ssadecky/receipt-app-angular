/**
 * Data Service - handling recipe tasks that are setting or
 * getting data type Recipe.
 */

import { EventEmitter, Injectable } from '@angular/core';

import { ShoppingListService } from './shoppinglist.service';
import { Recipe } from '../recipe-book/recipe.model';
import { Ingredient } from '../shared/ingredient.model';

@Injectable()

export class RecipeService {
  private recipes: Recipe[] = [
    new Recipe(
      'Spaghetti Carbonare',
      'Simple spaghetti recipe',
      'https://budgetbytes.com/wp-content/uploads/2016/05/Spaghetti-Carbonara-front-fork.jpg',
      [
        new Ingredient( 'Happiness', 1337),
        new Ingredient( 'LSD', 3)
      ]),
    new Recipe(
      'Tzatsssikkyy',
      'You may be surprised what you\'re in for',
      'http://thewanderlustkitchen.com/wp-content/uploads/2014/06/Authentic-Greek-Tzatziki-Dip-550.jpg',
      [
        new Ingredient( 'White ingredient', 200),
        new Ingredient( 'Green ingredient', 5)
      ]),
    new Recipe(
      'SS(p) Buns',
      'Ouh yea, those "buns" do look delicious',
      'http://www.simplyrecipes.com/wp-content/uploads/2013/12/st-lucia-buns-horiz-a-1800.jpg',
      [
        new Ingredient( 'Buns', 88),
        new Ingredient( 'Eggs', 3)
      ]),
  ];
  recipesChanged = new EventEmitter<Recipe[]>();
  recipeSelected = new EventEmitter<Recipe>();

  constructor( public shoppingListService: ShoppingListService ) {}

  getRecipes() {
    return this.recipes.slice();
  }

  addRecipe(recipe: Recipe) {
    this.recipes.push(recipe);
    this.recipesChanged.emit(this.recipes.slice());
  }

  addToShoppingList(ingredients: Ingredient[]) {
    this.shoppingListService.addIngredients(ingredients);
  }

}
