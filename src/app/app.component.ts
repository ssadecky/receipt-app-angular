import { Component, Input, OnInit } from '@angular/core';

import { ToggleService } from './shared/toggle.service';
import { DataService } from './shared/data.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: [ToggleService, DataService]
})
export class AppComponent {

  title: string = 'My cooking application';
  public recipes: boolean = true;
  service: string = 'recipe';

  constructor( private togglingService: ToggleService) { }

  ngOnInit() { }

  getService(service) {
    this.service = service;
  }

}
