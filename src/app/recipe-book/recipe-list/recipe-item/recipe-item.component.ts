import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';

import { Recipe } from '../../recipe.model';
import { RecipeService} from '../../../core/recipe.service';

@Component({
  selector: 'app-recipe-item',
  templateUrl: './recipe-item.component.html',
  styleUrls: ['./recipe-item.component.scss']
})
export class RecipeItemComponent implements OnInit {

  @Input() recipe: Recipe;

  constructor(private recipeService: RecipeService) {
    // code...
  }

  ngOnInit() {

  }

  onSelected() {
    this.recipeService.recipeSelected.emit(this.recipe);
  }


}
