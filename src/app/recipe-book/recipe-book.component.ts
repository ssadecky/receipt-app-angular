import { Component, OnInit, Input, OnChanges } from '@angular/core';

import { Recipe } from './recipe.model';
import { RecipeService } from '../core/recipe.service';

@Component({
  selector: 'app-recipe-book',
  templateUrl: './recipe-book.component.html',
  styleUrls: ['./recipe-book.component.scss']
})
export class RecipeBookComponent implements OnInit, OnChanges{

  recipeSelected: Recipe;

  constructor( private recipeService: RecipeService ) { }

  ngOnInit() {
    this.recipeService.recipeSelected
      .subscribe(
        (recipe: Recipe) => {
          this.recipeSelected = recipe;
        }
      );
  }

  ngOnChanges() { }

}
