import { Component, OnInit, ViewChild, ElementRef, EventEmitter, Output } from '@angular/core';

import { Ingredient } from './../../shared/ingredient.model';
import { ShoppingListService } from '../../core/shoppinglist.service';

@Component({
  selector: 'app-list-edit',
  templateUrl: './list-edit.component.html',
  styleUrls: ['./list-edit.component.scss']
})
export class ListEditComponent implements OnInit {

  @ViewChild('nameInput') nameInput: ElementRef;
  @ViewChild('amountInput') amountInput: ElementRef;

  constructor( private shoppingListService: ShoppingListService ) { }

  ngOnInit() {}

  addIngredient() {
    const name = this.nameInput.nativeElement.value;
    const amount = this.amountInput.nativeElement.value;
    const ingredient = new Ingredient( name, amount );
    this.shoppingListService.addIngredient(ingredient);
  }

  removeIngredient() {}

  clear() {}


}
