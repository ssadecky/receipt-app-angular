/**
 * Data Service - handling various tasks that are setting or
 * getting data type Recipes and Ingredients.
 */
import { EventEmitter } from '@angular/core';
import { Ingredient } from './ingredient.model';
import { Recipe } from '../recipe-book/recipe.model';

export class DataService {

  ingredientsChanged = new EventEmitter<Ingredient[]>();

  recipes: Recipe[] = [
    new Recipe(
      'Spaghetti Carbonare',
      'Simple spaghetti recipe',
      'https://budgetbytes.com/wp-content/uploads/2016/05/Spaghetti-Carbonara-front-fork.jpg'),
  ];
  ingredients: Ingredient[] = [
    new Ingredient('Peaches', 5),
    new Ingredient('Milk', 1)
  ];

  selectedRecipe: Recipe;

  getRecipes() {
    return this.recipes.slice();
  }
  getIngredients() {
    return this.ingredients.slice();
  }
  addIngredient(name: string, amount: number) {
    this.ingredients.push( new Ingredient(name, amount) );
  }
  showDetail( recipe: Recipe ) {
    this.selectedRecipe = recipe;
  }

}
