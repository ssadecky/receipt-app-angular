import {
  Directive,
  ElementRef,
  Renderer2,
  ViewContainerRef,
  Input,
  HostListener,
  HostBinding,
  Output
   } from '@angular/core';

@Directive({
  selector: '[appToggle]',
  exportAs: 'toggleDirective'
})
export class ToggleDirective {

  trigger: boolean = false;

  constructor(private elRef: ElementRef) { }

  @Input() appToggle;
  @HostListener('click') onClick() {
    this.trigger = true;
  }

  @HostListener('document:click', ['$event.target'])
  onOutsideClick(targetElement) {
    if (!(this.appToggle || this.elRef.nativeElement).contains(targetElement))
      this.trigger = false;
  }
}



  // getChildWithClass( element, className ) {
  //   let foundElement,
  //       found: boolean;

  //   function recurse( element, className: string, found:boolean ) {
  //     for (var i = 0; i < element.children.length && !found; i++) {

  //       var el = element.children[i];
  //       var classes = el.classList != undefined ? el.classList : [];

  //       if (classes.contains(className)) {
  //         found = true;
  //         foundElement = element.children[i];
  //         break;
  //       }
  //       recurse( element.children[i], className, found);
  //     }
  //   }

  //   recurse( element, className, false );

  //   return foundElement;
  // }

  // showControls() {
  //   console.log('clicked');
  //   this.isEditClicked = true;
  //   return this.isEditClicked;
  // }

